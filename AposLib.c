#include "AposLib.h"
#include <math.h>

#define MESES_POR_ANO 12

#ifdef __cplusplus
extern "C" {
#define APOS_LIB_CPP_NAMESPACE_PREFIX AposLib::
#else
#define APOS_LIB_CPP_NAMESPACE_PREFIX 
#endif

double APOS_LIB_CPP_NAMESPACE_PREFIX RendimentoMensalParaAnual(double rendimentoMensal)
{	
	return (pow( (100.+rendimentoMensal)/100., 12.)-1. )*100.;
}

double APOS_LIB_CPP_NAMESPACE_PREFIX RendimentoAnualParaMensal(double rendimentoAnual)
{
	return (pow( (100.+rendimentoAnual)/100., 1./12.)-1. )*100.;
}

double APOS_LIB_CPP_NAMESPACE_PREFIX CalcularTotalDepositado(double depositoMensal, int totalDeAnos)
{
	return totalDeAnos*MESES_POR_ANO*depositoMensal;
}

double APOS_LIB_CPP_NAMESPACE_PREFIX CalcularAposentadoria(double valorInicial, double depositoMensal, double rendimentoMensal, int quantidadeDeAnos)
{
	double valorAcumulado= valorInicial;
	double fatorDeRendimento= 1. + rendimentoMensal/100.;
	int numeroInteracoes= MESES_POR_ANO*quantidadeDeAnos;
	for(int i=0; i < numeroInteracoes; i++)
	{
		valorAcumulado*= fatorDeRendimento;
		valorAcumulado+= depositoMensal;
	}
	return valorAcumulado;
}

#ifdef __cplusplus
}
#endif

