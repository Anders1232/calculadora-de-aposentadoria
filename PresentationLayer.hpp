#ifndef PRESENTATIONLAYER_HPP
#define PRESENTATIONLAYER_HPP

#include <memory>
#include <gtkmm/window.h>
#include <gtkmm/spinbutton.h>

namespace CalculadoraDeAposentadoria
{
	namespace GTKMM_Frontend
	{
		class PresentationLayer
		{
			public:
				PresentationLayer() noexcept;
				~PresentationLayer ();
				Glib::RefPtr<Gtk::Window>& MainWindow(void);
			private:
				void RendimentoMensalAlterado(void);
				void RendimentoAnualAlterado(void);
				void AtualizarResultados(void);
				
				Glib::RefPtr<Gtk::Window> window;
				
				Glib::RefPtr<Gtk::SpinButton> sbValorInicial;
				Glib::RefPtr<Gtk::SpinButton> sbDepositoMensal;
				Glib::RefPtr<Gtk::SpinButton> sbRendimentoAnual;
				Glib::RefPtr<Gtk::SpinButton> sbRendimentoMensal;
				Glib::RefPtr<Gtk::SpinButton> sbAnosAcumulando;
				
				Glib::RefPtr<Gtk::Label> lbTotalDepositado;
				Glib::RefPtr<Gtk::Label> lbTotalAcumulado;
				Glib::RefPtr<Gtk::Label> lbRendimentoMensalFinal;
		};
	}
}

#endif // PRESENTATIONLAYER_HPP
