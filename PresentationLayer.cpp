#include "PresentationLayer.hpp"
#include "AposLib.h"
//#include <gtkmm/gtkmm.h>
#include <gtkmm/builder.h>
#include <gtkmm/layout.h>
#include <gtkmm/messagedialog.h>
#include <glibmm/fileutils.h>
#include <glibmm/markup.h>
#include <stdlib.h>
//#define DEV_STUFF
//#define SET_VALUE_TRIGGERS_ON_VALUE_CHANGE

#define USTR_WHERE ( ( (Glib::ustring(__FILE__) + " | ") + __func__) + ":") + std::to_string(__LINE__)

namespace CalculadoraDeAposentadoria
{
	namespace GTKMM_Frontend
	{
		PresentationLayer::PresentationLayer() noexcept
		{
			try
			{
				Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create_from_resource("/org/anders1232/calculadora-de-aposentadoria/gtkmm_frontend/JanelaPrincipal.glade");
				window = Glib::RefPtr<Gtk::Window>::cast_dynamic( builder->get_object("MAIN_WINDOW") );
				window->property_title().set_value("Calculadora de Aposentadoria");
				
				sbValorInicial= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic( builder->get_object("SPIN_BUTTON_VALOR_INICIAL") );
	//			sbValorInicial->set_update_policy(Gtk::SpinButtonUpdatePolicy::UPDATE_IF_VALID);
				sbValorInicial->set_value(2000);
				sbValorInicial->set_editable(true);
				
				sbDepositoMensal= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic( builder->get_object("SPIN_BUTTON_DEPOSITO_MENSAL") );
	//			sbDepositoMensal->set_update_policy(Gtk::SpinButtonUpdatePolicy::UPDATE_IF_VALID);
				sbDepositoMensal->set_value(200);
			//	sbDepositoMensal->set_editable(true);
				
				sbRendimentoAnual= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic( builder->get_object("SPIN_BUTTON_RENDIMENTO_ANUAL") );
	//			sbRendimentoAnual->set_update_policy(Gtk::SpinButtonUpdatePolicy::UPDATE_IF_VALID);
				sbRendimentoAnual->set_value(5);
			//	sbRendimentoAnual->set_editable(true);
				
				sbRendimentoMensal= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic( builder->get_object("SPIN_BUTTON_RENDIMENTO_MENSAL") );
	//			sbRendimentoMensal->set_update_policy(Gtk::SpinButtonUpdatePolicy::UPDATE_IF_VALID);
				sbRendimentoMensal->set_value(0.4074);
			//	sbRendimentoMensal->set_editable(true);
				
				sbAnosAcumulando= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic( builder->get_object("SPIN_BUTTON_ANOS_ACUMULANDO") );
	//			sbAnosAcumulando->set_update_policy(Gtk::SpinButtonUpdatePolicy::UPDATE_IF_VALID);
				sbAnosAcumulando->property_digits().set_value(0);
			//	sbAnosAcumulando->property_climb_rate().set_value(1);
			//	sbAnosAcumulando->set_increments(1, 2);
				sbAnosAcumulando->set_value(30);
			//	sbAnosAcumulando->set_editable(true);
				
				lbTotalDepositado= Glib::RefPtr<Gtk::Label>::cast_dynamic( builder->get_object("LABEL_RESULTADO_TOTAL_DEPOSITADO") );
				lbTotalDepositado->set_text("0,00");
				
				lbTotalAcumulado= Glib::RefPtr<Gtk::Label>::cast_dynamic( builder->get_object("LABEL_RESULTADO_TOTAL_ACUMULADO") );
				lbTotalAcumulado->set_text("0,00");
				
				lbRendimentoMensalFinal= Glib::RefPtr<Gtk::Label>::cast_dynamic( builder->get_object("LABEL_RESULTADO_RENDIMENTO_MENSAL_FINAL") );
				lbRendimentoMensalFinal->set_text("0,00");
				
				sbValorInicial->signal_value_changed().connect(sigc::mem_fun(*this, &PresentationLayer::AtualizarResultados) );
				sbDepositoMensal->signal_value_changed().connect(sigc::mem_fun(*this, &PresentationLayer::AtualizarResultados) );
				sbRendimentoAnual->signal_value_changed().connect(sigc::mem_fun(*this, &PresentationLayer::RendimentoAnualAlterado) );
				sbRendimentoMensal->signal_value_changed().connect(sigc::mem_fun(*this, &PresentationLayer::RendimentoMensalAlterado) );
				sbAnosAcumulando->signal_value_changed().connect(sigc::mem_fun(*this, &PresentationLayer::AtualizarResultados) );
				
				AtualizarResultados();
				window->present();
			}
			catch(Gtk::BuilderError& error)
			{
				Gtk::MessageDialog( USTR_WHERE + error.what() ,false, Gtk::MessageType::MESSAGE_ERROR );
				exit(EXIT_FAILURE);
			}
			catch(Glib::MarkupError& error)
			{
				Gtk::MessageDialog( USTR_WHERE + error.what(),false, Gtk::MessageType::MESSAGE_ERROR );
				exit(EXIT_FAILURE);
			}
			catch(Gio::ResourceError& error)
			{
				Gtk::MessageDialog( USTR_WHERE+error.what(),false, Gtk::MessageType::MESSAGE_ERROR );
				exit(EXIT_FAILURE);
			}
			
		}
		
		PresentationLayer::~PresentationLayer()
		{}
		
		void PresentationLayer::RendimentoAnualAlterado(void)
		{
			double novoValor= AposLib::RendimentoAnualParaMensal(sbRendimentoAnual->get_value() );
		#ifdef SET_VALUE_TRIGGERS_ON_VALUE_CHANGE
			if(sbRendimentoMensal->get_value() != novoValor)
		#endif
			{
				sbRendimentoMensal->set_value(novoValor);
			}
			AtualizarResultados();
		}
		
		void PresentationLayer::RendimentoMensalAlterado(void)
		{
			double novoValor= AposLib::RendimentoMensalParaAnual(sbRendimentoMensal->get_value() );
		#ifdef SET_VALUE_TRIGGERS_ON_VALUE_CHANGE
			if(sbRendimentoAnual->get_value() != novoValor)
		#endif
			{
				sbRendimentoAnual->set_value(novoValor);
			}
			AtualizarResultados();
		}
		
		void PresentationLayer::AtualizarResultados(void)
		{
			double valorInicial= sbValorInicial->get_value();
			double depositoMensal= sbDepositoMensal->get_value();
			double rendimentoMensal= sbRendimentoMensal->get_value();
			int anosAcumulando= sbAnosAcumulando->get_value_as_int();
			
			double totalDepositado= AposLib::CalcularTotalDepositado(depositoMensal, anosAcumulando);
			double totalAcmumulado= AposLib::CalcularAposentadoria(valorInicial, depositoMensal, rendimentoMensal, anosAcumulando);
			double rendimentoMensalFinal= totalAcmumulado*rendimentoMensal/100.;
			
			lbTotalDepositado->set_text(std::to_string(totalDepositado) );
			lbTotalAcumulado->set_text(std::to_string(totalAcmumulado) );
			lbRendimentoMensalFinal->set_text(std::to_string(rendimentoMensalFinal) );
			
		}
		
		Glib::RefPtr<Gtk::Window>& PresentationLayer::MainWindow(void)
		{
			return window;
		}
	}
}
