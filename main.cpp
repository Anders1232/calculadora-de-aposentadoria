#include "PresentationLayer.hpp"
#include <gtkmm/application.h>

int main (int argc, char **argv)
{
	Glib::RefPtr<Gtk::Application> app = Gtk::Application::create (argc, argv, "org.anders1232.aposentariometro");
	
	CalculadoraDeAposentadoria::GTKMM_Frontend::PresentationLayer frontEnd;
	
	return  app->run( *(frontEnd.MainWindow().get() ) );
}
