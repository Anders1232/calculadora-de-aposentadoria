#ifndef APOS_LIB_H
#define APOS_LIB_H

#ifdef __cplusplus

#ifndef DETECT_EXTERN_C_NAME_SPACE_COLISIONS
namespace AposLib{
#endif		//DETECT_EXTERN_C_NAME_SPACE_COLISIONS

extern "C" {
#endif		//__cplusplus

double RendimentoMensalParaAnual(double rendimentoMensal);

double RendimentoAnualParaMensal(double rendimentoAnual);

double CalcularTotalDepositado(double depositoMensal, int totalDeAnos);

double CalcularAposentadoria(double valorInicial, double depositoMensal, double rendimentoMensal, int quantidadeDeAnos);

#ifdef __cplusplus
}
#ifndef DETECT_EXTERN_C_NAME_SPACE_COLISIONS
}
#endif		//DETECT_EXTERN_C_NAME_SPACE_COLISIONS

#endif		//__cplusplus

#endif
